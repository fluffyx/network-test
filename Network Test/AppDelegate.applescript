--
--  AppDelegate.applescript
--

script AppDelegate
	property parent : class "NSObject"
    
    property timerCounter : 0
    property countDownTimer : missing value -- NSTimer

    property progressIndicator : missing value

    property timerDisplay : "Starting…"
    
    property wanPingPID : 0
    property lanPingPID : 0
    
    property rssi : ""
    property wirelessDataRate : ""
    property bssid : ""
    property ssid : ""
    property channel : ""
    property channelWidth : ""
    property channelGhz : ""
    property security : ""
    
    property speedTestNotYetRead : true
    property speedTestShouldStartAtCounter : missing value
	
	-- IBOutlets
    property theWindow : missing value
    
    property logView : missing value
    property logViewText : ""
    
    property packetsLanReceived : 0
    property packetsLanLost : 0
    property packetsWanReceived : 0
    property packetsWanLost : 0
    
    property wifiLabel : missing value
    property baseStationLabel : missing value
    property wifiSignalLabel : missing value
    property wifiChannelLabel : missing value
    property wifiChannelWidthLabel : missing value
    property wifiChannelGhzLabel : missing value
    property dataRateLabel : missing value
    property lanPingLabel : missing value
    property wanPingLabel : missing value
    property lanPingLostLabel : missing value
    property wanPingLostLabel : missing value
    property dnsLabel : missing value
    property downloadLabel : missing value
    property uploadLabel : missing value
    
    on setupDiagEnvironment()
        set folderpath to "~/.networktest/diagnostics/"
        do shell script "mkdir -p " & folderpath
        set folderpath to "~/.networktest/bin/"
        do shell script "mkdir -p " & folderpath
        if not doesFileExist("~/.networktest/bin/speedtest-cli")
            do shell script "cd ~/.networktest/bin/; curl -Lo speedtest-cli https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest.py; chmod +x speedtest-cli"
        end
        return true
    end setupDiagEnvironment
    
    on doesFileExist(path)
        set theResult to do shell script "if [ -e " & path & " ]; then echo \"exists\"; fi"
        if theResult is "exists" then
            return true
            else
            return false
        end if
    end doesFileExist
    
	on applicationWillFinishLaunching_(aNotification)
        setupDiagEnvironment()
	end applicationWillFinishLaunching_
    
    on applicationDidFinishLaunching_(aNotification)
        progressIndicator's startAnimation_(1)
        set countDownTimer to current application's NSTimer's scheduledTimerWithTimeInterval:0.1 target:me selector:"timerFired:" userInfo:(missing value) repeats:true
        set speedTestShouldStartAtCounter to 70 -- 7-ish seconds
        startWanPing()
        startLanPing()
    end
    
    on startWanPing()
        set cmd to "ping --apple-time -W10000 -i0.1 -c10 8.8.8.8 > ~/.networktest/diagnostics/pingwan.txt 2>&1 & echo $!"
        set wanPingPID to do shell script cmd
    end

    on doTheLog_(sender)
        log myDefaultGateway()
    end

    on wanPacketsReceived()
        try
            with timeout of 2 seconds
            set theLines to fileAsList("~/.networktest/diagnostics/pingwan.txt")
            set lossLine to item -3 of theLines
            set latencyLine to item -2 of theLines
            if latencyLine contains "100.0% packet loss"
                set packetsSent to (word 1 of latencyLine) as integer
                return {packetsSent,0,0,0,0,0}
            else if word -1 of latencyLine is "ms" and word 2 of lossLine is "packets" and word 6 of lossLine is "received" then
                set packetsSent to (word 1 of lossLine) as integer
                set packetsReceived to (word 4 of lossLine) as integer
                set min to (word -5 of latencyLine) as integer
                set avg to (word -4 of latencyLine) as integer
                set max to (word -3 of latencyLine) as integer
                set stddev to (word -2 of latencyLine) as integer
                return {packetsSent, packetsReceived, min, avg, max, stddev}
            else
                return false
            end if
        end timeout
        on error
            return false
        end try
    end

    on shellWithTimeout(timeout_in_seconds, command)
        do shell script "timeout() { time=$1; command=\"/bin/sh -c \\\"$2\\\"\"; expect -c \"set echo \\\"-noecho\\\"; set timeout $time; spawn -noecho $command; expect timeout { exit 1 } eof { exit 0 }\"; }; timeout " & timeout_in_seconds & " \"" & command & "\""
    end shellWithTimeout
    
    on startLanPing()
        set cmd to "ping --apple-time -W10000 -i0.1 -c10 " & myDefaultGateway() & " > ~/.networktest/diagnostics/pinglan.txt 2>&1 & echo $!"
        set lanPingPID to do shell script cmd
    end
    
    on lanPacketsReceived()
        try
            with timeout of 2 seconds
                set theLines to fileAsList("~/.networktest/diagnostics/pinglan.txt")
                set lossLine to item -3 of theLines
                set latencyLine to item -2 of theLines
                if latencyLine contains "100.0% packet loss"
                    set packetsSent to (word 1 of latencyLine) as integer
                    return {packetsSent,0,0,0,0,0}
                else if word -1 of latencyLine is "ms" and word 2 of lossLine is "packets" and word 6 of lossLine is "received" then
                    set packetsSent to (word 1 of lossLine) as integer
                    set packetsReceived to (word 4 of lossLine) as integer
                    set min to (word -5 of latencyLine) as integer
                    set avg to (word -4 of latencyLine) as integer
                    set max to (word -3 of latencyLine) as integer
                    set stddev to (word -2 of latencyLine) as integer
                    return {packetsSent, packetsReceived, min, avg, max, stddev}
                else
                    return false
                end if
            end timeout
        on error
            return false
        end try
    end

    on resetStatistics_(sender)
        set packetsLanReceived to 0
        set packetsLanLost to 0
        set packetsWanReceived to 0
        set packetsWanLost to 0
        lanPingLostLabel's setStringValue_("")
        -- lanPingLabel's setStringValue_("")
        wanPingLostLabel's setStringValue_("")
        -- wanPingLabel's setStringValue_("")
        -- dnsLabel's setStringValue_("")
    end

    on resetSpeedTest_(sender)
        if (uploadLabel's stringValue() as text) is not "Testing…"
            uploadLabel's setStringValue_("Testing…")
            downloadLabel's setStringValue_("Testing…")
            set speedTestShouldStartAtCounter to timerCounter + 10 -- 1 second
            set speedTestNotYetRead to true
        end
    end
    
    on logLanPing()
        set pingStats to lanPacketsReceived()
        if pingStats is not false
            set packetsSent to item 1 of pingStats
            set packetsReceived to item 2 of pingStats
            set pingAvg to item 4 of pingStats
            
            set packetsLanReceived to packetsLanReceived + packetsReceived
            set packetsLanLost to packetsLanLost + packetsSent - packetsReceived
            set prettyValue to "" & pingAvg & " milliseconds"
            if packetsLanLost > 0
                set lostPct to (round ((packetsLanLost / (packetsLanLost + packetsLanReceived)) * 1000)) / 10
                if lostPct is 0.0 then set lostPct to "≈ 0"
                lanPingLostLabel's setStringValue_("" & lostPct & "% (" & packetsLanLost & " packets)")
            else
                lanPingLostLabel's setStringValue_("None")
            end if

            if (lanPingLabel's stringValue() as string) is prettyValue then
                set prettyValue to replace_chars(prettyValue, " ", "  ")
            end

            lanPingLabel's setStringValue_(prettyValue)
            return true
        else
            return false
        end
    end

    on logWanPing()
        set pingStats to wanPacketsReceived()
        if pingStats is not false
            set packetsSent to item 1 of pingStats
            set packetsReceived to item 2 of pingStats
            set pingAvg to item 4 of pingStats
            
            set packetsWanReceived to packetsWanReceived + packetsReceived
            set packetsWanLost to packetsWanLost + packetsSent - packetsReceived
            set prettyValue to "" & pingAvg & " milliseconds"
            if packetsWanLost > 0
                set lostPct to (round ((packetsWanLost / (packetsWanLost + packetsWanReceived)) * 1000)) / 10
                if lostPct is 0.0 then set lostPct to "≈ 0"
                wanPingLostLabel's setStringValue_("" & lostPct & "% (" & packetsWanLost & " packets)")
            else
                wanPingLostLabel's setStringValue_("None")
            end if

            if (wanPingLabel's stringValue() as string) is prettyValue then
                set prettyValue to replace_chars(prettyValue, " ", "  ")
            end

            wanPingLabel's setStringValue_(prettyValue)

            return true
        else
            return false
        end
    end

    on runSpeedTest()
        shell("~/.networktest/bin/speedtest-cli --csv > ~/.networktest/diagnostics/speedtest.txt 2>&1 & echo $!")
    end runSpeedTest

    on readSpeedTest()
        set theRet to shell("tail -n3 ~/.networktest/diagnostics/speedtest.txt")
        set c to count of paragraphs of theRet
        if (theRet contains "nw_path_close_fd" and c is less than 2) or (theRet contains "nodename nor servname")
            return ""
        else
            return theRet
        end
    end

    on speedTestUpload(results)
        set r to ""
        if results contains "nw_path_close_fd"
            set r to paragraph 2 of results
        else
            set r to paragraph 2 of results
        end
        set r to first item of csvToList(r, {})
        set download to ((item 7 of r as integer) / 1000000) as integer
        set upload to ((item 8 of r as integer) / 1000000) as integer
        return upload
    end

    on speedTestDownload(results)
        set r to ""
        if results contains "nw_path_close_fd"
            set r to paragraph 2 of results
        else
            set r to paragraph 2 of results
        end
        set r to first item of csvToList(r, {})
        set download to ((item 7 of r as integer) / 1000000) as integer
        set upload to ((item 8 of r as integer) / 1000000) as integer
        return download
    end

    on resizeWindow_(sender)
        set theFrame to theWindow's frame
    end

    on timerFired_(sender)
        set timerCounter to timerCounter + 1
        
        if (speedTestNotYetRead is false) or (timerCounter is less than (speedTestShouldStartAtCounter - 20))
            if logWanPing() is true
                startWanPing()
            end

            if logLanPing() is true
                startLanPing()
            end
        end

        if timerCounter is speedTestShouldStartAtCounter
            runSpeedTest()
            downloadLabel's setStringValue_("Testing…")
            uploadLabel's setStringValue_("Testing…")
        else if speedTestNotYetRead is true and timerCounter is greater than (speedTestShouldStartAtCounter + 20)
            set theResults to readSpeedTest()
            if theResults is not ""
                set dl to "" & speedTestDownload(theResults) & " megabits/sec"
                set ul to "" & speedTestUpload(theResults) & " megabits/sec"
                downloadLabel's setStringValue_(dl)
                uploadLabel's setStringValue_(ul)
                set speedTestNotYetRead to false
            end
        end

        if timerCounter is 10
            set theResult to testDns()
            if theResult is not false
                set thePretty to "" & theResult & " milliseconds"
                dnsLabel's setStringValue_(thePretty)
            else
                dnsLabel's setStringValue_("Error")
            end
        end

        if timerCounter mod 10 is 0
            refreshWifiInfo()
        end
    end

    on millisecondsSinceEpoch()
        shell("python -c 'from time import time; print int(round(time() * 1000))'") as integer
    end millisecondsSinceEpoch

    on testDns()
        try
            return last paragraph of shell("starttime=$(python -c 'from time import time; print int(round(time() * 1000))');nslookup Apple.com;endtime=$(python -c 'from time import time; print int(round(time() * 1000))');(expr $endtime - $starttime)")
        on error
            return false
        end try
    end testDns

	on applicationShouldTerminate_(sender)
		--do shell script "kill -2 " & wanPingPID
        
		return current application's NSTerminateNow
	end applicationShouldTerminate_
    
    


    on refreshWifiInfo()
        set theWifiInfo to wifiInfo()
        set theLines to paragraphs of theWifiInfo
        repeat with thisLine in theLines
            set thisTextSplit to split(thisLine, ": ")
            if thisLine contains "agrCtlRSSI" then
                set rssi to item 2 of thisTextSplit
                wifiSignalLabel's setStringValue_(rssi & " dBm")
            else if thisLine contains "lastTxRate:" then
                set wirelessDataRate to item 2 of thisTextSplit
                dataRateLabel's setStringValue_(wirelessDataRate & " megabits/sec")
            else if thisLine contains "BSSID:" then
                set bssid to item 2 of thisTextSplit
                baseStationLabel's setStringValue_(bssid)
            else if thisLine contains "SSID:" then
                set ssid to item 2 of thisTextSplit
                wifiLabel's setStringValue_(ssid)
            else if thisLine contains "channel:" then
                set tempChannel to item 2 of thisTextSplit
                set tempList to first item of csvToList(tempChannel, {})
                set channel to first item of tempList
                if (channel as integer) is less than 12 then
                    set channelGhz to "2.4 GHz"
                    set prettyValue to "" & channel & " (" & channelGhz & ")"
else if (channel as integer) is greater than 11 then
                    set channelWidth to second item of tempList
                    set channelWidth to "" & channelWidth & " MHz"
                    set channelGhz to "5 GHz"
                    set prettyValue to "" & channel & " (" & channelGhz & ", " & channelWidth & ")"
                end
                wifiChannelLabel's setStringValue_(prettyValue)
            else if thisLine contains "link auth:" then
                set security to item 2 of thisTextSplit
            end if
        end repeat
    end

    on myDefaultGateway()
        first paragraph of (do shell script "regex=\"gateway: (.*)\";routes=`route get default`;[[ $routes =~ $regex ]] && echo \"${BASH_REMATCH[1]}\"")
    end myDefaultGateway

    on wifiInfo()
        shell("/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport --getinfo")
    end wifiInfo

    on shell(txt)
        do shell script txt
    end shell

    on a_shell(txt, output_file)
        my shell(txt & " > " & output_file & " 2>&1 & echo $!")
    end a_shell

    on splitText(someText, splitDelim)
        set AppleScript's text item delimiters to ": "
        set theArray to every word of someText
        return theArray
    end splitText

    to split(someText, delimiter)
        set AppleScript's text item delimiters to delimiter
        set someText to someText's text items
        set AppleScript's text item delimiters to {""} --> restore delimiters to default value
        return someText
    end split

    ----------
    
    
    
    
    on homePath()
        POSIX path of (path to home folder)
    end homePath
    on expandPath(the_path)
        if (characters 1 through 2 of the_path) is {"~", "/"} then
            set the_path to homePath() & (characters 3 through -1 of the_path)
        end if
        if character 1 of the_path is "~" then
            set the_path to homePath() & (characters 2 through -1 of the_path)
        end if
        the_path
    end expandPath


    on csvToList(csvText, implementation)
        -- The 'implementation' parameter is a record with optional properties specifying the field separator character and/or trimming state. The defaults are: {separator:",", trimming:false}.
        set {separator:separator, trimming:trimming} to (implementation & {separator:",", trimming:false})
        
        script o -- For fast list access.
            property textBlocks : missing value -- For the double-quote-delimited text item(s) of the CSV text.
            property possibleFields : missing value -- For the separator-delimited text items of a non-quoted block.
            property subpossibilities : missing value -- For the paragraphs of any non-quoted field candidate actually covering multiple records. (Single-column CSVs only.)
            property fieldsOfCurrentRecord : {} -- For the fields of the CSV record currently being processed.
            property finalResult : {} -- For the final list-of-lists result.
        end script

        set astid to AppleScript's text item delimiters

        considering case
            set AppleScript's text item delimiters to quote
            set o's textBlocks to csvText's text items
            -- o's textBlocks is a list of the CSV text's text items after delimitation with the double-quote character.
            -- Assuming the convention described at top of this script, the number of blocks is always odd.
            -- Even-numbered blocks, if any, are the unquoted contents of quoted fields (or parts thereof) and don't need parsing.
            -- Odd-numbered blocks are everything else. Empty strings in odd-numbered slots (except at the beginning and end) are due to escaped double-quotes in quoted fields.
            
            set blockCount to (count o's textBlocks)
            set escapedQuoteFound to false
            -- Parse the odd-numbered blocks only.
            repeat with i from 1 to blockCount by 2
                set thisBlock to item i of o's textBlocks
                if (((count thisBlock) > 0) or (i is blockCount)) then
                    -- Either this block is not "" or it's the last item in the list, so it's not due to an escaped double-quote. Add the quoted field just skipped (if any) to the field list for the current record.
                    if (escapedQuoteFound) then
                        -- The quoted field contained escaped double-quote(s) (now unescaped) and is spread over three or more blocks. Join the blocks, add the result to the current field list, and cancel the escapedQuoteFound flag.
                        set AppleScript's text item delimiters to ""
                        set end of o's fieldsOfCurrentRecord to (items quotedFieldStart thru (i - 1) of o's textBlocks) as text
                        set escapedQuoteFound to false
                        else if (i > 1) then -- (if this isn't the first block)
                        -- The preceding even-numbered block is an entire quoted field. Add it to the current field list as is.
                        set end of o's fieldsOfCurrentRecord to item (i - 1) of o's textBlocks
                    end if
                    
                    -- Now parse the current block's separator-delimited text items, which are either complete non-quoted fields, stubs from the removal of quoted fields, or still-joined fields from adjacent records.
                    set AppleScript's text item delimiters to separator
                    set o's possibleFields to thisBlock's text items
                    set possibleFieldCount to (count o's possibleFields)
                    repeat with j from 1 to possibleFieldCount
                        set thisPossibleField to item j of o's possibleFields
                        set c to (count thisPossibleField each paragraph)
                        if (c < 2) then
                            -- This possible field doesn't contain a line break. If it's not the stub of a preceding or following quoted field, add it (trimmed if trimming) to the current field list.
                            -- It's not a stub if it's an inner candidate from the block, the last candidate from the last block, the first candidate from the first block, or it contains non-white characters.
                            if (((j > 1) and ((j < possibleFieldCount) or (i is blockCount))) or ((j is 1) and (i is 1)) or (notBlank(thisPossibleField))) then set end of o's fieldsOfCurrentRecord to trim(thisPossibleField, trimming)
                            else if (c is 2) then -- Special-cased for efficiency.
                            -- This possible field contains a line break, so it's really two possible fields from consecutive records. Split it.
                            set subpossibility1 to paragraph 1 of thisPossibleField
                            set subpossibility2 to paragraph 2 of thisPossibleField
                            -- If the first subpossibility's not just the stub of a preceding quoted field, add it to the field list for the current record.
                            if ((j > 1) or (i is 1) or (notBlank(subpossibility1))) then set end of o's fieldsOfCurrentRecord to trim(subpossibility1, trimming)
                            -- Add the now-complete field list to the final result list and start one for a new record.
                            set end of o's finalResult to o's fieldsOfCurrentRecord
                            set o's fieldsOfCurrentRecord to {}
                            -- If the second subpossibility's not the stub of a following quoted field, add it to the new list.
                            if ((j < possibleFieldCount) or (notBlank(subpossibility2))) then set end of o's fieldsOfCurrentRecord to trim(subpossibility2, trimming)
                            else
                            -- This possible field contains more than one line break, so it's three or more possible fields from consecutive single-field records. Split it.
                            set o's subpossibilities to thisPossibleField's paragraphs
                            -- With each subpossibility except the last, complete the field list for the current record and initialise another. Omit the first subpossibility if it's just the stub of a preceding quoted field.
                            repeat with k from 1 to c - 1
                                set thisSubpossibility to item k of o's subpossibilities
                                if ((k > 1) or (j > 1) or (i is 1) or (notBlank(thisSubpossibility))) then set end of o's fieldsOfCurrentRecord to trim(thisSubpossibility, trimming)
                                set end of o's finalResult to o's fieldsOfCurrentRecord
                                set o's fieldsOfCurrentRecord to {}
                            end repeat
                            -- With the last subpossibility, just add it to the new field list (if it's not the stub of a following quoted field).
                            set thisSubpossibility to end of o's subpossibilities
                            if ((j < possibleFieldCount) or (notBlank(thisSubpossibility))) then set end of o's fieldsOfCurrentRecord to trim(thisSubpossibility, trimming)
                        end if
                    end repeat
                    
                    -- Otherwise, the current block's an empty text item due to either an escaped double-quote in a quoted field or the opening quote of a quoted field at the very beginning of the CSV text.
                    else if (escapedQuoteFound) then
                    -- It's another escaped double-quote in a quoted field already flagged as containing one. Just replace the empty text with a literal double-quote.
                    set item i of o's textBlocks to quote
                    else if (i > 1) then -- (if this isn't the first block)
                    -- It's the first escaped double-quote in a quoted field. Replace the empty text with a literal double-quote, note the index of the preceding even-numbered block (the first part of the field), and flag the find.
                    set item i of o's textBlocks to quote
                    set quotedFieldStart to i - 1
                    set escapedQuoteFound to true
                end if
            end repeat
        end considering

        set AppleScript's text item delimiters to astid

        -- Add the remaining field list to the output if it's not empty or if the output list itself has remained empty.
        if ((o's fieldsOfCurrentRecord is not {}) or (o's finalResult is {})) then set end of o's finalResult to o's fieldsOfCurrentRecord

        return o's finalResult
    end csvToList

    -- Test whether or not a string contains any non-white characters.
    on notBlank(txt)
        ignoring white space
            return (txt > "")
        end ignoring
    end notBlank

    -- Trim any leading or trailing spaces from a string.
    on trim(txt, trimming)
        if (trimming) then
            set c to (count txt)
            repeat while ((txt begins with space) and (c > 1))
                set txt to text 2 thru -1 of txt
                set c to c - 1
            end repeat
            repeat while ((txt ends with space) and (c > 1))
                set txt to text 1 thru -2 of txt
                set c to c - 1
            end repeat
            if (txt is space) then set txt to ""
        end if
        
        return txt
    end trim

    on fileAsList(theFile)
        try
            return paragraphs of textFromFile(theFile)
        on error
            return false
        end
    end fileAsList

    on textFromFile(_file) -- This reads a WAN ping file reliably, which AppleScript can't (!!)
        try
            set _file to expandPath(_file)
            set _path to _file's POSIX path
            set _nsString to current application's NSString's stringWithContentsOfFile:_path usedEncoding:(missing value) |error|:(missing value)
            return _nsString as text
        on error
            return false
        end
    end textFromFile

    on replace_chars(this_text, search_string, replacement_string)
        set AppleScript's text item delimiters to the search_string
        set the item_list to every text item of this_text
        set AppleScript's text item delimiters to the replacement_string
        set this_text to the item_list as string
        set AppleScript's text item delimiters to ""
        return this_text
    end replace_chars

	
end script
